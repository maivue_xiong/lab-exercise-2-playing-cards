
// Playing Cards
// Mai Vue Xiong

#include <iostream>
#include <conio.h>

using namespace std;

enum class Rank
{
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13,
	Ace = 14
};

enum class Suit
{
	Clubs,
	Diamonds,
	Hearts,
	Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{


	(void)_getch();
	return 0;
}
